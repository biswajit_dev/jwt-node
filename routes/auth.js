const express = require("express");
const router = express.Router();

const { register, login, userList } = require("../controller/User");
const { verify } = require("../middleware");

router.post("/register", register);
router.post("/login", login);
router.get('/userlist', verify, userList);

module.exports = router;

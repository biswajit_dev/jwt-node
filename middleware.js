const jwt = require("jsonwebtoken");

const verify = (req, res, next) => {
    const token = req.header('auth-token');
    if(!token) return res.status(401).send({message: "Access deny"});
    try {
        const verifyToken = jwt.verify(token, process.env.TOKEN_SECRET);
        req.user = verifyToken;
        next();
    } catch (error) {
     return res.status(400).send({message: "Invalid token"});   
    }
}

module.exports = {
    verify
}
const express = require("express");
const app = express();
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
require("dotenv").config();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

//connect db
mongoose.connect(
  process.env.DB_CONNECT,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  () => console.log("connected to db!")
);

//Import router
const authRoute = require("./routes/auth");

//use router
app.use("/api/user", authRoute);

const PORT = process.env.PORT || 4000;

app.listen(PORT, console.log("server running at port: 4000"));

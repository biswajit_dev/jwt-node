const User = require("../model/User");
const bcrypt = require("bcrypt");
const Joi = require("joi");
const jwt = require("jsonwebtoken");

const saltRounds = 10;

const encodePassword = async (password) => {
  const encryptedPsw = await bcrypt.hash(password, saltRounds);
  return encryptedPsw;
};

const registerSchema = Joi.object({
  name: Joi.string().required(),
  password: Joi.string().pattern(new RegExp("^[a-zA-Z0-9]{3,30}$")),
  email: Joi.string().email({
    minDomainSegments: 2,
    tlds: { allow: ["com", "net"] },
  }),
});

const loginSchema = Joi.object({
  password: Joi.string().required(),
  email: Joi.string().email({
    minDomainSegments: 2,
    tlds: { allow: ["com", "net"] },
  }),
});

exports.register = async (req, res) => {
  const { error } = await registerSchema.validate(req.body);
  if (error) return res.status(400).send({ message: error.details[0].message });
  const user = new User({
    name: req.body.name,
    email: req.body.email,
    password: await encodePassword(req.body.password),
  });
  try {
    const userData = await User.findOne({ email: req.body.email });
    if (!userData) {
      const savedUser = await user.save();
      return res.status(200).send(savedUser);
    } else {
      return res.status(400).send({ message: "User already exist" });
    }
  } catch (error) {
    return res.status(400).send({ message: "Internal server error" });
  }
};

exports.login = async (req, res) => {
  const { error } = await loginSchema.validate(req.body);
  if (error) return res.status(400).send({ message: error.details[0].message });
  try {
    const user = await User.findOne({ email: req.body.email });
    if (!user) return res.status(400).send({ message: "User dosen't exist" });
    const validPass = await bcrypt.compare(req.body.password, user.password);
    if (!validPass)
      return res.status(400).send({ message: "Email or password is wrong" });

    //Create and sign token
    const token = jwt.sign({ userId: user._id }, process.env.TOKEN_SECRET);
    console.log(token);
    return res.status(200).header("auth-token", token).send({
      name: user.name,
      email: user.email,
    });
  } catch (error) {
    return res.status(400).send({ message: "Internal server error" });
  }
};

exports.userList = async (req, res) => {
  try {
    const userDetails = await User.find();
    return res.status(200).send({data: userDetails});
  } catch (error) {
    return res.status(400).send({ message: "Internal server error" });
  }
};
